# Build Strapido executable
FROM golang:alpine as builder
ARG STRAPI_URL
WORKDIR /src
ADD strapido ./
RUN mkdir -p /src/public/static && \
    cp -r /src/static/* /src/public/static && \
    cp -r /src/errorpages/* /src/public/

RUN go mod download && \
    go build -o strapido cmd/app.go

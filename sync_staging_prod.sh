#!/bin/bash
PROD="hed@82.165.121.129"

if [ -d ~/ddnsking ]; then

	echo "Sync database"
	echo "scp ~/ddnsking/liebfine/strapi/app/.tmp/data.db ${PROD}:/home/hed/liebfine/strapi/app/.tmp"
	scp ~/ddnsking/liebfine/strapi/app/.tmp/data.db ${PROD}:/home/hed/liebfine/strapi/app/.tmp

	echo "Delete old uploads"
	echo "ssh ${PROD}  'rm -rf /home/hed/liebfine/strapi/app/public/uploads/*'"
	ssh ${PROD}  'rm -rf /home/hed/liebfine/strapi/app/public/uploads/*'

	echo "Sync files"
	echo "scp -r ~/ddnsking/liebfine/strapi/app/public/uploads/* ${PROD}:/home/hed/liebfine/strapi/public/uploads/"
	scp -r ~/ddnsking/liebfine/strapi/app/public/uploads/* ${PROD}:/home/hed/liebfine/strapi/app/public/uploads/
else
	echo "must be executed on Kaom Staging server"
fi

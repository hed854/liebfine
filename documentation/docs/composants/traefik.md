# Traefik

Traefik est un reverse proxy en Golang, il permet de sélectionner les backends à exposer, à protéger et d'ajouter des fonctionnalités comme le HTTPS ou la gestion d'erreur.

## Configuration

Traefik peut se configurer de multiples façons (labels Docker, fichiers TOML etc). Dans le projet Liebfine, Traefik est configuré entièrement via le File Provider, avec des configurations YAML.

**Chaque environnement possède ses confs Traefik**

Deux types de config:

**La config statique:** `traefik.<env>.yaml`. Référence la conf dynamique en File Provider et renseigne les ports d'écoute.

**La config dynamique:** `dynamic_config/config.<env>.yaml`.

* **Routers:**

    * `strapiRouter` expose Strapi (API, Admin), toujours sur le port 1337
    * `strapidoRouter` expose le frontend en HTTPS. Par défaut, il s'agit de Varnish.
    * `grafanaRouter` expose le dashboard Grafana

* **Middlewares:**

    * Gestion d'erreur 404 et 500
    * uniquement pour l'environnement PROD: protection Basic Auth pour Prometheus

* **Services (backends):** on référence directement les services Docker.

**Représentation réseau**

![](../images/liebfine_network_dev.svg)

## Gestion HTTPS

HTTPS est installé sur tous les environnements Strapi. Tous les certificats sont dans `dynamic_config/certs`.

!!! warning "Versioning et certificats"

    Par sécurité, la clé privée du certificat n'est jamais versionnée dans Git. Elle doit être déposée manuellement dans le répertoire certs.

!!! warning "Traefik ne génère pas lui-même ces certificats?"

    Pour le projet Liebfine il a été choisi de générer les certificats en-dehors de Traefik afin de ne pas trop être couplé au produit. En DEV et STAGING, MkCert est utilisé (voir Cookbook)

## Cookbook

??? info "Passer Traefik en debug"

    Dans la configuration statique, ajouter:

    ```
    log:
      level: DEBUG
    ```

    Note: l'environnement DEV est déjà en mode debug

??? info "Créer des certificats pour DEV et STAGING"

    1. Installer [Mkcert](https://github.com/FiloSottile/mkcert) sur le poste de développement. Sur Archlinux par exemple: `sudo pacman -S mkcert`
    2. Générer les certificats dans le répertoire `certs`

    ```
    mkcert --install <domain.name>
    mv *.pem reverse_proxy/dynamic_config/certs
    ```

??? info "Renouveler les certificats en PROD"

    1. Se connecter sur la PROD.
    2. Transférer les certificats (.cer et .key) dans le répertoire `liebfine/reverse_proxy/dynamic_config/certs`

    ```
    scp liebfine.com_private_key.key <user>@liebfine.com:/home/<user>/liebfine/reverse_proxy/dynamic_config/certs
    scp liebfine.com_ssl_certificate.cer <user>@liebfine.com:/home/<user>/liebfine/reverse_proxy/dynamic_config/certs
    ```

    3. Redémarrer Traefik

    ```
    docker-compose -f prod.yaml restart reverse_proxy
    ```


## Annexes

### Fonctionnement Traefik

![](https://doc.traefik.io/traefik/v2.2/assets/img/architecture-overview.png)

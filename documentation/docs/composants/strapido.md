# Strapido

Strapido est un générateur de site statique développé maison en Golang. 

![](../images/strapido.png)

Lorsque Strapido est lancé, les actions suivantes sont réalisées:

1. Requêtage l'API Strapi
2. Enrichissement des données
    
    * Affine les tris si besoin
    * Ajoute des champs calculés
3. Rendu des templates dans le répertoire `public/` 
4. HTTP Serve du répertoire `public/` :arrow_left: Strapi reste dans cet état jusqu'à son extinction

!!! warning "Génération statique"

    De par son fonctionnement statique, Strapido nécessite les points d'attention suivants

    * **Si des modifications sont effectuées côté Strapi**, elles ne seront PAS prises en compte dans Strapido. Il faudra redémarrer Strapi pour que les modifications soient prises en compte via un nouveau rendu.
    * **Strapido ne démarre pas lors d'un premier lancement docker-compose:** car aucune orchestration n'a été mise en place pour que Strapido attende la fin du démarrage de Strapi (Strapi est + lent à démarrer que Strapido). Il faut donc relancer Strapido via `docker-compose <env>.yaml up -d` (une fois que Strapi a correctement démarré).

## Templates frontend

Les templates Strapido sont des templates Golang (`html/template`)

## Pages d'erreur

Les pages d'erreur de Liebfine sont incluses dans le répertoire Strapido mais n'ont aucune dépendance au moteur de template Strapido. Elles sont directement configurées au niveau de Traefik pour être servie en cas d'erreur.

## Librairie frontend

Strapido utilise Bootstrap 5 et les icones Bootstrap qui vont avec. La totalité du code est tiré des exemples dans la documentation Bootstrap.

!!! warning

    Bootstrap est embarqué en dur dans le répertoire `static` de Strapido. Il faut le mettre à jour de temps en temps.

## Cookbook

??? info "Démarrer Strapido en mode verbose"

    Le mode verbose sert à debugger le process de génération statique. Il s'active dans le fichier docker-compose <env>.yaml:

    ```
      strapido:
        networks:
          - liebfine
        depends_on:
          - strapi
        // ... blabla
        command: ./strapido --verbose 
    ```



# Varnish

Varnish est utilisé pour cacher en mémoire les pages HTTP de Strapido. Etant donné que Strapido ne génère les pages qu'à la demande, la hit rate entre chaque déploiement devrait être proche de 100%.

## Configuration

`/varnish/default.vcl`:

* pose les headers cache basiques 
* pose des headers `X-Cache` pratiques pour le debug.
* retire certains headers inutiles ou gênants pour le cache (:warning: quand la gestion des utilisateurs sera en place, il faudra certainement gérer le header Cookie différemment)

Varnish est activé directement **sur tous les environnements** en tant que serveur backend du `strapiService` dans la configuration dynamique de Traefik.

Côté Docker on alloue 500M de tmpfs, ce qui est largement suffisant pour la volumétrie actuelle.

## Cookbook

??? info "Bypasser Varnish en développement"
    
    Dans le fichier `config.<env>.yaml` (configuration dynamique Traefik), réactiver `http://strapi:1337` en serveur backend pour le `strapiService`, au lieu de Varnish.

    ```
      services:
        strapiService:
          loadBalancer:
            servers:
              # use varnish
              - url: "http://varnish"
              # bypass varnish
              #- url: "http://strapi:1337"
    ```

??? info "Voir les informations de hit/miss"

    **Méthode 1:** observer la valeur des headers `X-Cache` dans la réponse. Utiliser la debug console du navigateur, ou varnishlog.

    ```
    docker-compose -f <env>.yaml exec varnish varnishlog
    ```

    **Méthode 2:** varnishstat

    ```
    docker-compose -f <env>.yaml exec varnish varnishstat
    ```



??? info "Reset le cache à zéro"

    Pour remettre le cache à zéro sans redémarrer Varnish (ce qui ferait tomber le site).

    ```
    docker-compose -f <env>.yaml exec varnish varnishadm "ban req.url ~ /"
    ```

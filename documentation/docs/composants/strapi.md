# Strapi

[Strapi](https://strapi.io/) est un CMS headless en NodeJS. Il permet de générer facilement un modèle de données personnalisé ainsi qu'une API REST.

![](../images/strapi.png)

* Aucune customization n'a été réalisée sur Strapi pour Liebfine
* Aucune connaissance NodeJS n'est nécessaire pour mettre en place Strapi
* La base utilisée est la base par défaut SQLite

## Configuration

* Configuration BDD: `app/config/database.js`
* BDD Sqlite: `app/.tmp/data.db`

## API REST

L'API REST est consultable sur `<endpoint>:1337/courses` (exemple pour la resource courses)

## Cookbook

??? info "Exporter les définitions de contenu"
    
    ```
    docker-compose exec strapi strapi config:dump -p -f config.json
    ```

??? info "Accéder directement à la BDD Sqlite"

    ```
    sqlite3 app/.tmp/data.db
    ```

??? info "Mettre à jour la version de Strapi" 

    ```
    docker-compose pull
    ```

    La commande téléchargera la dernière version de toutes les images, y compris celle de Strapi

    A faire sur chaque environnement qui a besoin d'une mise à jour.

??? info "Ajouter et déployer un nouveau champ"

    1. Ajouter le nouveau champ sur l'environnement DEV. Vérifier si les permissions Strapi permettent de voir le champ! (tester en se connectant sur un compte non-admin par exemple).
    2. Pusher le commit dans Git
    3. Pull le change dans l'enviro STAGING.

??? info "Changer le mot de passe de l'admin UI"
    
    ```
    docker-compose -f <env>.yaml exec strapi strapi admin:reset-user-password`
    ```

??? info "Mettre à jour la BDD de prod avec les données de staging"

    1. Se connecter sur le staging en SSH.
    2. Lancer le script `sync_staging_prod.sh`
    3. Quand la synchro est terminée, relancer Strapido pour générer une nouvelle version du site.

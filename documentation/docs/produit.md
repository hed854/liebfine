# Vision produit

## Cas d'utilisation

Acteurs:

* **Utilisateur:** un visiteur sur le site web (via ordinateur ou tablette ou mobile). 
* **Administrateur:** la personne qui écrit les fiches Formation ou les pages statiques pour mettre à jour la base de données Liebfine.

Cas d'utilisation:

* **Rédaction**: quand l'administrateur met à jour la base de données Liebfine via l'interface d'administration.
* **Tris**: quand l'utilisateur choisit différents critères pour trouver la formation qui lui convient
* **Détails d'une formation**: quand l'utilisateur clique sur une formation pour trouver les informations détaillées de celle-ci.
* **Conversion**: quand l'utilisateur Liebfine a acheté une formation chez le partenaire. :warning: la conversion n'est pas complète lorsque l'utilisateur clique sur un bouton "Acheter la formation" sur le site de Liebfine.

## Stratégie

**Etape 0:** proof of concept sous Wordpress **[terminé: Juillet 2021]**

* Ecriture des premiers contenus
* Affiner les rubriques et les critères de comparaison

**Etape 1:** lot MVP avec les fonctionnalités suivantes: **[terminé: Décembre 2021]**

* Page de listing des formations
* Fiches détaillées des formations en immobilier
* Pages statiques sur l'investissement immobilier
* Mise en place d'une base Métier
* Mise en place d'un système pour mettre à jour la base Métier

En requirements non-fonctionnels, le MVP doit répondre aux besoins suivants:

* Mise en place d'un CMS avec des types de contenus personnalisés au lieu de se baser sur le modèle Wordpress
* Mise en place d'une API afin de pouvoir faire évoluer le frontend facilement
* Fournir une performance plus élevée que Wordpress (réduire le time to first bytes à moins de 500ms)
* Mise en place d'un environnement de DEV pour développer les nouvelles fonctionnalités
* Mise en place d'un environnement de Staging pour valider les nouvelles fonctionnalités avant de les mettre en rod 

**Etape 2:** améliorations continues du Frontend

* Rendre le frontend plus accessible pour l'utilisateur (ex: tris dynamiques)

## Maquettes

Les maquettes sont disponibles sur Figma.


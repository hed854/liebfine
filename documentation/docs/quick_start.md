# Démarrage rapide

## Développer sur Liebfine

Démarrer rapidement un environnement de DEV Liebfine

### Prérequis 

* Docker 20.x
* Docker-Compose 2.x

### Installation

1. Cloner la branche default (`main`) du repository git
2. Créer le réseau Docker: `docker network create liebfine`
3. Ajouter `127.0.0.1 liebfine.local` au fichier hosts
4. Lancer `docker-compose -f dev.yaml up -d`
5. Vérifier que tous les services tournent correctement: `docker-compose -f dev.yaml ps`

!!! Note

    On peut aussi lancer un seul service docker-compose en faisant `docker-compose -f dev.yaml up -d <service>`. (utile par exemple pour éditer juste la Documentation)

### Accéder aux services

Pour connaître les ports accessibles, lire le fichier `dev.yaml` ou utiliser `docker-compose -f dev.yaml ps`. La liste donnée ci-dessous n'est pas exhaustive.

|Site|Rôle|Commentaires|
|----|----|------------|
| [liebfine.local](https://liebfine.local) | Frontend Strapido ||
| [liebfine.local:1337](https://liebfine.local:1337) | Backend Strapi ||
| [liebfine.local:1337/courses](https://liebfine.local:1337/courses) | API Strapi (resource `courses/`) ||
| [liebfine.local:1338](https://liebfine.local:1338) | Dashboard Grafana ||
| [localhost:8080](http://localhost:8080) | Dashboard Traefik ||
| [localhost:9090](http://localhost:9090) | Dashboard Prometheus ||
| [localhost:8000](http://localhost:8000) | Documentation Mkdocs ||

## Déployer une modif DEV en STAGING

En DEV:

1. Créer une branche `git checkout -b <feature>` et push les modifications concernant la modif. 
2. Créer une Merge Request pour que le changement soit visible et code reviewable.

Sur STAGING:

1. Se placer sur la branche `<feature>` (`git fetch --all && git checkout <feature>`)
2. Mettre à jour les images de staging: `docker-compose -f staging.yaml pull`
3. Regénérer un frontend Strapido avec les nouvelles données: `docker-compose -f staging.yaml build --no-cache` 
3. Relancer les services avec les nouvelles versions des images et de Strapido: `docker-compose -f staging.yaml up -d` 
4. [Si Strapido a été trop rapide à se lancer et a planté](/composants/strapido): `docker-compose -f staging.yaml up -d` une nouvelle fois

## Déployer en PROD

!!! warning

    De façon générale, éviter de déployer des branches en PROD. Essayer d'avoir terminé le travail de recette et code review afin de toujours travailler sur `main`.

Sur STAGING:

1. (optionnel) Envoyer les contenus vers la prod en utilisant `./sync_staging_prod.sh`

Sur PROD:

1. Récupérer les modifs de code `git pull`
2. Mettre à jour les images de prod: `docker-compose -f prod.yaml pull`
3. Regénérer un frontend Strapido avec les nouvelles données: `docker-compose -f prod.yaml build --no-cache` 
3. Relancer les services avec les nouvelles versions des images et de Strapido: `docker-compose -f prod.yaml up -d` 
4. [Si Strapido a été trop rapide à se lancer et a planté](/composants/strapido): `docker-compose -f prod.yaml up -d` une nouvelle fois

# Accueil

!!! info "Liebfine, c'est quoi?"

    **Liebfine est un comparateur de formations en immobilier** fondé par Serge Rakotoharisoa en 2021. La société Liebfine SARL compte également une chaîne Youtube et des comptes de réseaux sociaux. 

    Revue de presse: [Youtube](https://www.youtube.com/channel/UC3RtVvpbOpXdao6FAGOXNOw), [Interview France Bleu](https://www.francebleu.fr/emissions/nouvelle-eco/touraine/liebfine-un-comparateur-pour-apprendre-a-investir-dans-l-immobilier)

## Le site web

Le site web comporte 3 composants principaux:

* **le backend Strapi:** un CMS headless en NodeJS qui fournit une interface d'administration et une API REST de contenu
* **le frontend Strapido:** un générateur de frontend statique (développement maison en Golang)
* **le reverse proxy Traefik:** permet de protéger et abstraire l'accès aux différents composants (gestion des domaines, HTTPS, basic auth etc)  

En savoir plus:

[Architecture](architecture){ .md-button .md-button--primary }

## Environnements

Le site est déployé sur 3 environnements différents. Pour plus d'informations sur la partie déploiement, voir la section Exploitation.

|Environment|Backend|Frontend|
|-----------|-------|--------|
| `dev` | https://liebfine.local:1337 | https://liebfine.local |
| `staging` | https://kaom.ddnsking.com:1337 | https://kaom.ddnsking.com |
| `prod` | http://www.liebfine.com:1337 | http://www.liebfine.com |

En savoir plus:

[Infrastructure](infrastructure){ .md-button .md-button--primary }

## Doc développeur

[Démarrage rapide](quick_start){ .md-button .md-button--primary }

* Pour démarrer rapidement en tant que développeur Liebfine, utiliser le Démarrage Rapide.
* Pour apprendre à utiliser les différents composants de Liebfine, lire la section Composants

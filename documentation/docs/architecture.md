# Architecture

## Fonctionnement simplifié

Liebfine est architecturé autour du backend Strapi. 

* Strapi est un CMS Headless qui fournit à la fois une interface d'admin et une API REST HTTP.
* Le Frontend se connecte à l'API backend pour construire les pages HTML.

![](images/liebfine_simple.svg)

!!! success "Avantages"

    * La technologie Frontend peut être modifiée sans que l'expérience de l'Administrateur change
    * Se reposer sur un CMS existant = pas d'effort de développement complexe à fournir pour la partie CRUD / Authentification / Base de données / création de l'API REST

!!! danger "Désavantages"

    * La BDD SQLite est très basique et nécessitera possiblement un upgrade vers PostgreSQL/MariaDB (notamment si la nouvelle version du Frontend n'est plus générée statiquement + le nombre de visites augmente)



## Fonctionnement complet 

!!! info 

    * Pour prendre connaissance de l'architecture complète, il est conseillé de consulter les fichiers Docker-Compose `dev.yaml`, `staging.yaml` ou `prod.yaml`.
    * Les environnements DEV, STAGING et PROD ont de légères différences de fonctionnement, indiquées dans  les fichiers docker-compose et dans la [section Infrastructure de la documentation](/infrastructure)


### Liste complète des composants

|Composant|Produit|Répertoire de config|Commentaire|
|---------|-------|----------|-----------|
|Backend | **[Strapi](/composants/strapi)** |`strapi/`|CMS headless en NodeJS| 
|BDD | SQLite |`strapi/`| Intégré au composant backend|
|Frontend|**[Strapido](/composants/strapido)**|`strapido/`| Générateur statique maison en Golang|
|Reverse proxy | **[Traefik](/composants/traefik)** | `reverse_proxy/`||
|Proxy cache | **[Varnish](/composants/varnish)** |`varnish/`|Cache HTTP des pages|
|Metrics TSDB|Prometheus|`observability/`|Récupère les metrics des différents composants|
|Metrics Dashboard |Grafana|`observability/`|Affiche les metrics|
|Documentation|MkDocs|`documentation/`||
|Dynamic DNS|ddns-updater|`ddns/`|Uniquement utilisé sur l'enviro Staging|


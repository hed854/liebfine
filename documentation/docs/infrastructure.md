# Infrastructure

L'infrastructure Liebfine est entièrement basée sur Docker et Docker-Compose.

## Enviro DEV

L'environnement DEV est totalement indépendant des autres environnements.

L'enviro DEV utilise des certificats self-signed (Mkcert)

## Enviro STAGING

!!! warning 

    L'environnement de STAGING devra être recrée sur un nouveau serveur au choix lors de la passation du code Liebfine.

L'enviro STAGING est un serveur maison Archlinux, exposé sur Internet via un DDNS. L'environnement STAGING est nécessaire pour:
    
* Supervision Grafana: l'instance Grafana est utiliseé à la fois pour STAGING et PROD.
* Recette des modifications 
* Mise à jour des contenus: lorsque le contenu est mis à jour sur STAGING, il est poussé en PROD via le script `sync_staging_prod.sh` 

L'enviro STAGING utilise des certificats self-signed (Mkcert)

L'accès SSH via clé doit être activé pour le développeur.

## Enviro PROD

L'enviro PROD est un VPS 1and1/Ionos provisionné (manuellement) avec Docker et Docker-Compose.

L'enviro PROD utilise des certificats signés fournis par Liebfine SARL.

L'accès SSH via clé doit être activé pour le développeur.

Aucune modification depuis l'admin Strapi n'est effectuée en PROD. Tout est fait en STAGING puis transféré en PROD.

## Interaction entre environnements

En bleu: stack web

En jaune: stack de supervision

![](images/liebfine_env_interaction.svg)

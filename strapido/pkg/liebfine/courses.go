package liebfine

import (
	"time"
)

type Course struct {
	Config                SiteConfig
	IsCourse              bool
	Id                    int                 `json:"id"`
	Title                 string              `json:"title"`
	Author                string              `json:"author"`
	Pricing               float64             `json:"pricing"`
	HasCpf                bool                `json:"hasCpf"`
	HasQualiopi           bool                `json:"hasQualiopi"`
	Duration              int                 `json:"duration"`
	Modules               int                 `json:"modules"`
	IsSpecialized         bool                `json:"isSpecialized"`
	HasUpdates            bool                `json:"hasUpdates"`
	IsUnlocked            bool                `json:"isUnlocked"`
	UnlockingDetails      string              `json:"unlockingDetails"`
	GuaranteeDetails      string              `json:"guaranteeDetails"`
	HasPaymentInstalments bool                `json:"hasPaymentInstalments"`
	AvailabilityDetails   string              `json:"availabilityDetails"`
	CourseDetails         string              `json:"courseDetails"`
	PaymentDetails        string              `json:"paymentDetails"`
	IncludedTools         string              `json:"includedTools"`
	IncludedServices      string              `json:"includedServices"`
	InvestingSteps        []InvestingStep     `json:"investing_steps"`
	PropertyTypes         []PropertyType      `json:"property_types"`
	InvestingStrategies   []InvestingStrategy `json:"investing_strategies"`
	CourseMaterials       []CourseMaterial    `json:"course_materials"`
	OtherStrategies       []OtherStrategy     `json:"other_strategies"`
	PurchaseUrl           string              `json:"purchaseUrl"`
	Cover                 Cover               `json:"cover"`
	UpdatedAt             time.Time           `json:"updated_at"`
	Communities           []Community         `json:"communities"`
	// Calculated fields
	Slug               string
	PageTitle          string
	HasCoaching        bool
	HasDiscussionGroup bool
	DurationOrModules  string
}

type InvestingStep struct {
	Name     string `json:"name"`
	FullName string `json:"fullName"`
}

type PropertyType struct {
	Name     string `json:"name"`
	FullName string `json:"fullName"`
}

type InvestingStrategy struct {
	Name     string `json:"name"`
	FullName string `json:"fullName"`
}

type CourseMaterial struct {
	Name     string `json:"name"`
	FullName string `json:"fullName"`
}

type OtherStrategy struct {
	Name     string `json:"name"`
	FullName string `json:"fullName"`
}

type Community struct {
	Name     string `json:"name"`
	FullName string `json:"fullName"`
}

type Cover struct {
	Url          string            `json:"url"`
	CoverFormats map[string]Format `json:"formats"`
}

type Format struct {
	Url string `json:"url"`
}

type SiteConfig struct {
	ApiHostname         string
	WwwHostname         string
	ApiInternalHostname string
}

type CourseList struct {
	Config                    SiteConfig
	IsCourse                  bool
	PageTitle                 string
	SpecializedCourses        []Course
	UnspecializedCourses      []Course
	CPFCourses                []Course
	CertifiedCourses          []Course
	ColocationCourses         []Course
	InvestmentPropertyCourses []Course
}

package liebfine

import (
	"time"
)

type Static struct {
	Config    SiteConfig
	IsCourse  bool
	Id        int       `json:"id"`
	Title     string    `json:"title"`
	Content   string    `json:"content"`
	UpdatedAt time.Time `json:"updated_at"`
	// Calculated fields
	Slug      string
	PageTitle string
}

module strapido

go 1.16

require (
	github.com/gomarkdown/markdown v0.0.0-20210918233619-6c1113f12c4a // indirect
	github.com/gosimple/slug v1.10.0
	github.com/spf13/pflag v1.0.5 // indirect
)

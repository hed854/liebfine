package main

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"github.com/gomarkdown/markdown"
	"github.com/gosimple/slug"
	flag "github.com/spf13/pflag"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	lf "strapido/pkg/liebfine"
	"strings"
	"time"
)

// Page is a representation of the HTML file
type Page struct {
	// io lib only works with byte slices
	HtmlBody []byte
}

func (p *Page) writeHtml(slug string) error {
	filename := fmt.Sprintf("public/%s.html", slug)
	return ioutil.WriteFile(filename, p.HtmlBody, 0600)
}

func render(tpl *template.Template, data interface{}, filename string) {

	buffer := new(bytes.Buffer)
	err := tpl.Execute(buffer, data)
	if err != nil {
		log.Fatalf(err.Error())
	}

	// Write page file
	page := Page{buffer.Bytes()}
	err = page.writeHtml(filename)
	if err != nil {
		log.Fatalf(err.Error())
	}
}

func getEnvDefault(environmentVariable string, defaultValue string) string {
	result := defaultValue
	if tempEnv := os.Getenv(environmentVariable); tempEnv != "" {
		result = tempEnv
	}
	return result
}

func filterColocation(courses []lf.Course, verbosePtr *bool) []lf.Course {
	var filteredCourses []lf.Course
	for _, course := range courses {
		for _, item := range course.InvestingStrategies {
			if strings.Contains(strings.ToLower(item.Name), "coloc") {
				filteredCourses = append(filteredCourses, course)
			}
		}
	}

	if *verbosePtr {
		log.Printf(">> colocation filter: %d found", len(filteredCourses))
	}

	return filteredCourses
}

func filterInvestmentProperty(courses []lf.Course, verbosePtr *bool) []lf.Course {
	var filteredCourses []lf.Course
	for _, course := range courses {
		for _, item := range course.PropertyTypes {
			// Immeubles de rapport
			if strings.Contains(strings.ToLower(item.Name), "immeubles") {
				filteredCourses = append(filteredCourses, course)
			}
		}
	}

	if *verbosePtr {
		log.Printf(">> investment property filter: %d found", len(filteredCourses))
	}

	return filteredCourses
}

func requestCourses(route string, config lf.SiteConfig, verbosePtr *bool) []lf.Course {

	var rawCourses []lf.Course
	var resultCourses []lf.Course

	GetContent(
		fmt.Sprintf("%s/%s", config.ApiInternalHostname, route),
		verbosePtr,
		&rawCourses)

	for _, item := range rawCourses {
		// Raw enrich (calculated fields)
		item.Config = config
		item.Slug = fmt.Sprintf("%d-%s", item.Id, slug.Make(item.Title))
		item.PageTitle = fmt.Sprintf("%s par %s", item.Title, item.Author)
		item.DurationOrModules = calculateDurationOrModules(item.Duration, item.Modules)

		resultCourses = append(resultCourses, item)
	}

	return resultCourses
}

func renderIndividualCourses(route string, config lf.SiteConfig, tpl_funcs template.FuncMap, verbosePtr *bool) {

	// Will be returned to create the index page
	//indexBlocks := make(map[string][]lf.Course)

	// Parse courses
	var rawCourses []lf.Course
	//var specializedCourses []lf.Course
	//var unspecializedCourses []lf.Course

	GetContent(
		fmt.Sprintf("%s/%s", config.ApiInternalHostname, route),
		verbosePtr,
		&rawCourses)

	// Render individual pages
	tpl_course := template.Must(template.New("layout.html").Funcs(tpl_funcs).ParseFiles(
		"templates/layout.html",
		"templates/course.html",
		"templates/course_hero.html",
		"templates/course_characteristics.html",
		"templates/course_contents.html",
		"templates/table_checkbox.html",
	))
	for _, item := range rawCourses {
		if *verbosePtr {
			fmt.Printf("> processing: %s\n", item.Title)
		}
		// Raw enrich (calculated fields)
		item.Slug = fmt.Sprintf("%d-%s", item.Id, slug.Make(item.Title))
		item.PageTitle = fmt.Sprintf("%s par %s", item.Title, item.Author)
		item.Config = config
		item.IsCourse = true
		for _, community := range item.Communities {
			switch community.Name {
			case "coaching":
				item.HasCoaching = true
			case "groupe privé":
				item.HasDiscussionGroup = true
			default:
				item.HasCoaching = false
				item.HasDiscussionGroup = false
			}
		}

		item.DurationOrModules = calculateDurationOrModules(item.Duration, item.Modules)

		// Render
		render(tpl_course, item, item.Slug)
	}
}

func main() {
	Banner()
	startTime := time.Now()

	// Flags
	var verbosePtr = flag.Bool("verbose", false, "if true, verbose mode")
	var portPtr = flag.Int("port", 80, "server port")
	flag.Parse()

	// image links
	siteConfig := lf.SiteConfig{
		getEnvDefault("API_PUBLIC_URL", "https://liebfine.local:1337"),
		getEnvDefault("WWW_PUBLIC_URL", "https://liebfine.local"),
		getEnvDefault("API_INTERNAL_URL", "http://strapi:1337"),
	}

	log.Printf("API_INTERNAL_URL: %v\n", siteConfig.ApiInternalHostname)
	log.Printf("API_PUBLIC_URL: %v\n", siteConfig.ApiHostname)
	log.Printf("WWW_PUBLIC_URL: %v\n", siteConfig.WwwHostname)

	// template functions
	tpl_funcs := template.FuncMap{
		"markdown": fromMarkdown,
	}

	// Courses individual pages
	renderIndividualCourses("courses", siteConfig, tpl_funcs, verbosePtr)

	// Filter tabs
	// Warning: filter syntax will change in Strapi v4.
	// These are v3 filters, please make sure you're reading the right documentation version.
	tabSpecialized := requestCourses("courses?isSpecialized=true", siteConfig, verbosePtr)
	tabUnspecialized := requestCourses("courses?isSpecialized=false", siteConfig, verbosePtr)
	tabCPF := requestCourses("courses?hasCpf=true", siteConfig, verbosePtr)
	tabCertified := requestCourses("courses?hasQualiopi=true", siteConfig, verbosePtr)
	tabColocation := filterColocation(requestCourses("courses", siteConfig, verbosePtr), verbosePtr)
	tabInvestProperties := filterInvestmentProperty(requestCourses("courses", siteConfig, verbosePtr), verbosePtr)

	// Index page
	courseList := lf.CourseList{siteConfig, false, "Accueil", tabSpecialized, tabUnspecialized, tabCPF, tabCertified, tabColocation, tabInvestProperties}
	tpl_list := template.Must(template.ParseFiles("templates/layout.html", "templates/list.html", "templates/list_tab.html"))
	render(tpl_list, courseList, "index")

	// Static pages
	var rawStaticPages []lf.Static
	GetContent(
		fmt.Sprintf("%s/static-pages", siteConfig.ApiInternalHostname),
		verbosePtr,
		&rawStaticPages)

	tpl_static := template.Must(template.New("layout.html").Funcs(tpl_funcs).ParseFiles(
		"templates/layout.html",
		"templates/static.html",
	))

	for _, item := range rawStaticPages {
		// Raw enrich (calculated fields)
		item.Slug = slug.Make(item.Title)
		item.PageTitle = item.Title
		item.IsCourse = false
		item.Config = siteConfig
		if *verbosePtr {
			fmt.Printf("> processing: %s (%s)\n", item.Title, item.Slug)
		}
		render(tpl_static, item, item.Slug)
	}

	// Status all
	elapsedTime := time.Since(startTime)
	log.Printf("Verbose flag: %v\n", *verbosePtr)
	//log.Printf("Total files rendered: %d\n", len(rawCourses)+1)
	log.Printf("Total rendering time: %s\n", elapsedTime)

	fileServer := http.FileServer(http.Dir("public/"))
	http.Handle("/", fileServer)
	log.Printf("Serving files on port %d\n", *portPtr)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *portPtr), nil))

}

func fromMarkdown(field string) template.HTML {
	// replace headers to avoid H1s
	updatedField := strings.Replace(field, "#", "###", -1)
	// render markdown
	s := string(markdown.ToHTML([]byte(updatedField), nil, nil))
	return template.HTML(s)
}

func calculateDurationOrModules(duration int, modules int) string {
	output := ""
	if duration > 0 {
		output = fmt.Sprintf("%dh de vidéo", duration)
	} else if modules > 0 {
		if modules == 1 {
			output = "1 module"
		} else {
			output = fmt.Sprintf("%d modules", modules)
		}
	}
	return output
}

func GetContent(url string, verbosePtr *bool, content interface{}) {
	if *verbosePtr {
		log.Printf("> http call: %s\n", url)
	}
	// Setup HTTP Client with insecure
	// (used on environments where the build is not done on the CA server, ie netlify, gitlab etc)
	httpClient := &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	// Query API
	resp, err := httpClient.Get(url)
	if err != nil {
		log.Fatalf(err.Error())
	}
	if resp.StatusCode != 200 {
		log.Fatalf("Error! %s: %s", url, resp.Status)
	}
	body, err := ioutil.ReadAll(resp.Body)

	// Unmarshal response
	err = json.Unmarshal([]byte(body), &content)
	if err != nil {
		log.Fatalf(err.Error())
	}
}

func Banner() {
	log.Println(`
 _____ ___________  ___  ______ ___________ _____
/  ___|_   _| ___ \/ _ \ | ___ \_   _|  _  \  _  |
\ '--.  | | | |_/ / /_\ \| |_/ / | | | | | | | | |
 '--. \ | | |    /|  _  ||  __/  | | | | | | | | |
/\__/ / | | | |\ \| | | || |    _| |_| |/ /\ \_/ /
\____/  \_/ \_| \_\_| |_/\_|    \___/|___/  \___/`)
}

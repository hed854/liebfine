# Strapido

Simple static generator on top of Strapi API.

As of now, strapido cannot be run with local `go run app --port xxxx`. Please use the docker-compose file to get a fully configured strapi and strapido using internal docker network to get data (+ the automatic `static` dir move).



vcl 4.0;

backend default {
  .host = "strapi";
  .port = "1337";
}

sub vcl_recv {
   unset req.http.set-cookie;
   unset req.http.cookie;
}

sub vcl_backend_response {
   unset beresp.http.Cache-Control;
   set beresp.http.Cache-Control = "public, max-age=86400";
   set beresp.ttl = 1d;
}

sub vcl_deliver {
  if (obj.hits > 0) { # Add debug header to see if it's a HIT/MISS and the number of hits, disable when not needed
    set resp.http.X-Cache = "HIT";
  } else {
    set resp.http.X-Cache = "MISS";
  }

  unset resp.http.X-Powered-By;
  unset resp.http.X-Varnish;
  unset resp.http.Set-Cookie;
  unset resp.http.Cookie;
}
